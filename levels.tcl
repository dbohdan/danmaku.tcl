namespace eval ::danmaku::levels {
    namespace export *
    namespace path ::danmaku
}

namespace eval ::danmaku::levels::1 {
    namespace path {
        ::danmaku
        ::danmaku::levels
    }
}

proc ::danmaku::levels::1::init scale {
    lassign [image::zoom-subsample $scale] zoom subsample
    log debug [list background zoom $zoom subsample $subsample]

    # Background.
    image::load-scale $zoom \
                      $subsample \
                      assets/background-foggy.png \
                      background
    background copy background -from 0 \
                                     0 \
                                     [expr { round($scale * 384) }] \
                                     [expr { round($scale * 448) }] \
                               -to 0 \
                                   [expr { round($scale * 448) }] \
                                   [expr { round($scale * 384) }] \
                                   [expr { round($scale * 2 * 448) }]


    .danmaku.c create image 0 \
                            0 \
                            -image background \
                            -tag background

    # Shots.
    lassign [image::zoom-subsample $scale 10.0] \
            shotZoom shotSubsample
    log debug [list shot zoom $shotZoom subsample $shotSubsample]
    image::load-scale $shotZoom $shotSubsample assets/shot-green.png shot-green


    image::load-scale {*}[image::zoom-subsample $scale 4.0] \
                      assets/dart.png \
                      dart

    # Player.

    set playerX [expr { $scale * (384 - 128 / 4) / 2.0 }]
    set playerY [expr { $scale * 318 }]
    set playerSize [expr { $scale * 5 }]

    image::load-scale {*}[image::zoom-subsample $scale 4.0] \
                      assets/player.png \
                      player

    .danmaku.c create image 0 \
                            0 \
                            -anchor center \
                            -image player \
                            -tag {player playerSprite}

    .danmaku.c create rect 0 \
                           0 \
                           $playerSize \
                           $playerSize \
                           -fill red \
                           -outline {} \
                           -tag {player playerHitbox}

    .danmaku.c moveto playerHitbox [expr { $scale * -3 }] \
                                   [expr { $scale * -5 }]

    return [list $playerX $playerY $playerSize]
}

proc ::danmaku::levels::1 {} {
    set scale $::danmaku::state(scale)

    lassign [1::init $scale] playerX playerY playerSize

    set frame [yield [info coroutine]]
    set invuln 100

    while 1 {
        # Scroll background.

        .danmaku.c moveto background 0 [expr {
            $scale * (round(0.4 * $frame) % 448 - 448)
        }]

        # Player motion.

        lassign [controls::handle-input 2.0 $playerX $playerY] playerX playerY

        .danmaku.c moveto player $playerX $playerY

        # Spawn patterns.

        if {$frame % 500 == 1} {
            add-entity [coroutine pattern-spiral-$frame patterns::spiral \
                10 \
                [expr { $scale * 192 }] \
                [expr { $scale * 224 }] \
                25 \
            ] pattern
        }

        if {$frame % 500 in {100 200 300}} {
            set pat [coroutine pattern-line-$frame patterns::line shot-green 6]
            add-entity $pat pattern
        }


        if {$frame % 1000 == 250} {
            set pat [coroutine pattern-line-$frame patterns::drop dart 4 3]
            add-entity $pat pattern
        }

        # Hit detection.

        if {$invuln} {
            incr invuln -1

            if {$invuln / 20 % 2} {
                .danmaku.c itemconfigure playerHitbox -fill white
            } else {
                .danmaku.c itemconfigure playerHitbox -fill red
            }

            if {$invuln == 0} {
                log debug {end of invulnerability}
            }
        } else {
            set bbox [.danmaku.c bbox playerHitbox]
            set overlapping [.danmaku.c find overlapping {*}$bbox]

            set enemyShots [.danmaku.c find withtag enemyShot]

            foreach item $overlapping {
                if {$item ni $enemyShots} continue

                log debug [list collision with $item]

                incr ::danmaku::state(lives) -1
                set invuln 250

                break
            }
        }

        set frame [yield]
    }
}
