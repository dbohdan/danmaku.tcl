default: run

run: build
	tclsh danmaku.tcl

build: danmaku.tcl

danmaku.tcl: danmaku-dev.tcl levels.tcl
	tclsh tools/assemble.tcl danmaku-dev.tcl > danmaku.tcl
	-chmod +x danmaku.tcl

clean:
	-rm danmaku.tcl

.PHONY: build clean default run
