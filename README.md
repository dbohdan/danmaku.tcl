# danmaku.tcl

A small Tcl/Tk 8.6 sprite-based game demo inspired by the [Touhou Project](https://en.wikipedia.org/wiki/Touhou_Project) and [Glider](https://en.wikipedia.org/wiki/Glider_(video_game)).  The code is intended to be straightforward.

## How to run

```sh
tclsh danmaku-dev.tcl [window-scaling-multiplier]
```
