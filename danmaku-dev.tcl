#! /usr/bin/env tclsh
# Copyright (c) 2019, 2021, 2024 D. Bohdan
# License: MIT
package require Tcl 8.6 9
package require Tk

namespace eval ::danmaku {
    variable version 0.1.0

    variable state

    set state(cursorX) 0
    set state(cursorY) 0
    set state(keys) {
        left 0
        up 0
        down 0
        right 0
        mouse 0
        esc 0
    }
    set state(mouseControl) 0
}

interp alias {} ::source+ {} ::source
source+ controls.tcl
source+ image.tcl
source+ levels.tcl
source+ patterns.tcl

proc ::danmaku::log {level message} {
    set timestamp [clock format [clock seconds] \
                                -format %Y-%m-%dT%H:%M:%S]
    puts stderr "$timestamp ($level) $message"
}

proc ::danmaku::add-entity {callback label {misc {}}} {
    variable state

    log debug [list add-entity $callback $label]

    dict set misc callback $callback
    dict set misc label $label
    dict set state(entities) $state(entityCounter) $misc
    incr state(entityCounter)
}

proc ::danmaku::loop {} {
    variable state

    # Counters.
    incr state(frame)
    set state(currentT) [clock seconds]
    set state(elapsedT) [expr { $state(currentT) - $state(startT) }]

    # Call level event coroutine.
    level $state(frame)

    # Call the coroutines for entities.
    foreach key [dict keys $state(entities)] {
        set callback [dict get $state(entities) $key callback]

        $callback $state(frame)
        if {[info commands $callback] eq {}} {
            log debug [list removing entity $key]
            dict unset state(entities) $key
        }
    }

    if {$state(frame) % 100 == 0} {
        if { ![info exists state(fpsT)] } {
            set state(fpsT) 0
        }
        set fps [expr { 100 / ($state(elapsedT) - $state(fpsT)) }]
        set state(fpsT) $state(elapsedT)

        .danmaku.if itemconfigure fps -text "FPS: $fps"
        .danmaku.if itemconfigure lives -text "Player: $state(lives)"
    }

    after 20 ::danmaku::loop
}

proc ::danmaku::level n {
    variable state

    log debug [list level $n]

    .danmaku.c delete all

    set state(entityCounter) 0
    set state(entities) {}
    set state(level) $n

    coroutine level levels::$state(level)

    set state(startT) [clock seconds]
}

proc ::danmaku::gcd {a b} {
    if {$a == $b} {
        return $a
    }

    if {$a > $b} {
        tailcall gcd [expr {$a - $b}] $b
    } else {
        tailcall gcd $a [expr {$b - $a}]
    }
}

proc ::danmaku::keysym-to-action keysym {
    switch $keysym {
        a -
        Left {
            return left
        }
        w -
        Up {
            return up
        }
        Right -
        d {
            return right
        }
        Down -
        s {
            return down
        }
        m {
            return mouse
        }
        Escape {
            return esc
        }
        default {
            return {}
        }
    }
}

proc ::danmaku::init {{scaleArg 1}} {
    variable state

    if {!([string is double -strict $scaleArg]
          && $scaleArg > 0
          && $scaleArg <= 10
          && abs($scaleArg) != Inf
          && [lindex [regexp -all -inline {\.*(\d+?)0*$} $scaleArg] 1]
             in {1 2 3 4 5 6 7 8 9 25 75})} {
        error [list $scaleArg is not a valid scaling number]
    }

    set state(scale) [expr { round($scaleArg * 20) / 20.0 }]

    set screenWidth [expr { round($state(scale) * 640) }]
    set screenHeight [expr { round($state(scale) * 480) }]

    toplevel .danmaku
    wm title .danmaku danmaku.tcl
    wm resizable .danmaku 0 0
    wm geometry .danmaku =${screenWidth}x$screenHeight

    # Interface.

    font create danmakuOSD -family Arial \
                           -weight bold \
                           -size [expr { round($state(scale) * 15) }]

    place [canvas .danmaku.if \
                  -width $screenWidth \
                  -height $screenHeight \
                  -bg black \
                  -highlightthickness 0] \
          -x 0 \
          -y 0

    set ib [image::load-scale {*}[image::zoom-subsample $state(scale)] \
                              assets/interface-backdrop.png]

    .danmaku.if create image [expr { $state(scale) * 320 }] \
                             [expr { $state(scale) * 240 }] \
                             -image $ib

    .danmaku.if create text \
        [expr { $state(scale) * 430 }] \
        [expr { $state(scale) * 20 }] \
        -anchor nw \
        -fill white \
        -font danmakuOSD \
        -tag {osd fps} \
        -text {FPS: 00}

    .danmaku.if create text \
        [expr { $state(scale) * 430 }] \
        [expr { $state(scale) * 48 }] \
        -anchor nw \
        -fill white \
        -font danmakuOSD \
        -tag {osd lives} \
        -text {Player: 00}

    # Playing field.

    set fieldWidth [expr { round($state(scale) * 384) }]
    set feildHeight [expr { round($state(scale) * 448) }]

    place [canvas .danmaku.c \
                  -width $fieldWidth \
                  -height $feildHeight \
                  -bg black \
                  -highlightthickness 0] \
          -x [expr { round($state(scale) * 32) }] \
          -y [expr { round($state(scale) * 16) }]

    bind .danmaku <Motion> {
        set ::danmaku::state(cursorX) %x
        set ::danmaku::state(cursorY) %y
    }
    bind .danmaku <KeyPress> {
        set action [::danmaku::keysym-to-action %K]
        dict set ::danmaku::state(keys) $action 1
    }
    bind .danmaku <KeyRelease> {
        set action [::danmaku::keysym-to-action %K]
        dict set ::danmaku::state(keys) $action 0
    }

    set state(lives) 5
}

proc ::danmaku::main argv {
    wm withdraw .

    ::danmaku::init {*}$argv
    bind .danmaku <Destroy> { exit 0 }

    ::danmaku::level 1
    after 0 ::danmaku::loop
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    ::danmaku::main $argv
}
