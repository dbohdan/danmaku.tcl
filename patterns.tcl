namespace eval ::danmaku::patterns {
    namespace export *
}

proc ::danmaku::patterns::init {} {
    uplevel 1 {
        set scale $::danmaku::state(scale)

        set items {}

        set startFrame [yield [info coroutine]]
        set frame $startFrame
        set d 0

        set tag [info coroutine]
    }
}

proc ::danmaku::patterns::destroy {} {
    uplevel 1 {
        .danmaku.c delete {*}[dict values $items]
    }
}

proc ::danmaku::patterns::offscreen {margin x y} {
    return [expr {
        $x < -$margin ||
        $x > 384 + $margin ||
        $y < -$margin ||
        $y > 448 + $margin
    }]
}

proc ::danmaku::patterns::line {sprite n} {
    init

    set size [expr { $scale * 10 }]

    for {set i 0} {$i <= $n} {incr i} {
        set x [expr { $scale * 384 * $i / $n }]
        set y -$size

        set x2 [expr { $x + $size }]
        set y2 [expr { $y + $size }]

        dict set items $i [.danmaku.c create image \
            $x $y \
            -image $sprite \
            -tag [list $tag enemyShot] \
        ]
    }


    while {$d < 1000} {
        set d [expr { $frame - $startFrame }]

        set x [expr { $scale * sin($d * 0.03141592) * 384 / $n }]
        set y [expr { $scale * $d - $size }]

        .danmaku.c moveto $tag $x $y

        set frame [yield]
    }

    destroy
}

proc ::danmaku::patterns::drop {sprite n {speed 2} {delay 10}} {
    init

    set size [expr { $scale * 10 }]

    set coords {}

    set total 0
    for {set wave 1} {$wave <= $n} {incr wave} {
        set div [expr { 2 ** $wave }]

        for {set i 1} {$i < $div} {incr i 2} {
            set x [expr { $scale * 384 * $i / $div }]
            set y -$size

            dict set coords $total x $x
            dict set coords $total y $y

            dict set items $total [.danmaku.c create image \
                $x \
                $y \
                -image $sprite \
                -tag [list $tag enemyShot] \
            ]

            incr total
        }
    }


    while {$d < 1000} {
        set d [expr { $frame - $startFrame }]

        dict for {i item} $items {
            if {$i > $d * (1.0 / $delay)} break

            set x [dict get $coords $i x]
            set y [dict get $coords $i y]

            set y [expr { $y + $scale * $speed }]
            dict set coords $i y $y

            .danmaku.c moveto $item $x $y
        }


        set frame [yield]
    }

    destroy
}

proc ::danmaku::patterns::spiral {n origX origY {interval 25}} {
    init

    set size [expr { $scale * 12 }]

    for {set i 0} {$i <= $n} {incr i} {
        set x $origX
        set y $origY

        set x2 [expr {$x + $size}]
        set y2 [expr {$y + $size}]

        dict set items $i [.danmaku.c create oval \
            $x $y \
            $x2 $y2 \
            -fill yellow \
            -outline {} \
            -tag [list $tag enemyShot] \
        ]
    }

    while 1 {
        set d [expr { $frame - $startFrame }]

        dict for {i item} $items {
            set t [expr { max(0, $d - $i * $interval) }]

            set x [expr {
                $origX + $scale * $t ** 1.8 * 0.01 * cos($t * 0.03141592)
            }]
            set y [expr {
                $origY + $scale * $t ** 1.8 * 0.01 * sin($t * 0.03141592)
            }]

            .danmaku.c moveto $item $x $y

            if {[offscreen [expr { $size + $scale * 300 }] $x $y]} {
                dict unset items $i
            }
        }

        if {[dict size $items] == 0} break

        set frame [yield]
    }

    destroy
}
