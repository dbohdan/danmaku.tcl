namespace eval ::danmaku::controls {}

proc ::danmaku::controls::move-towards {v x y destX destY} {
    if {$destX == $x && $destY == $y} {
        return [list $x $y]
    }

    set alpha [expr {
        atan2($destY - $y, $x - $destX)
    }]

    set newX [expr { $x - $v * cos($alpha) }]
    set newY [expr { $y + $v * sin($alpha) }]

    return [list $newX $newY]
}

proc ::danmaku::controls::handle-keyboard-system {} {
    set input $::danmaku::state(keys)

    if {[dict get $input esc]} {
        exit 0
    }

    if {[dict get $input mouse]} {
        set ::danmaku::state(mouseControl) [expr {
            !$::danmaku::state(mouseControl)
        }]
    }
}

proc ::danmaku::controls::handle-mouse-movement {v x y} {
    if {!$::danmaku::state(mouseControl)} {
        return [list $x $y]
    }

    tailcall move-towards $v \
                          $x \
                          $y \
                          $::danmaku::state(cursorX) \
                          $::danmaku::state(cursorY)
}

proc ::danmaku::controls::handle-keyboard-movement {v x y} {
    set input $::danmaku::state(keys)

    set destX $x
    if {[dict get $input left]} {
        set destX [expr { $x - 10 }]
    } elseif {[dict get $input right]} {
        set destX [expr { $x + 10 }]
    }

    set destY $y
    if {[dict get $input up]} {
        set destY [expr { $y - 10 }]
    } elseif {[dict get $input down]} {
        set destY [expr { $y + 10 }]
    }

    tailcall move-towards $v $x $y $destX $destY
}

proc ::danmaku::controls::handle-input {v x y} {
    handle-keyboard-system

    if {$::danmaku::state(mouseControl)} {
        tailcall handle-mouse-movement $v $x $y
    } else {
        tailcall handle-keyboard-movement $v $x $y
    }
}
