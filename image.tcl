namespace eval ::danmaku::image {}

# Idea courtesy RS on https://wiki.tcl-lang.org/page/Image+scaling.
proc ::danmaku::image::zoom-subsample {scale {spriteScale 1.0}} {
    set scale [expr { round($scale * 20 / $spriteScale) / 20.0 }]
    set a [expr { int($scale * 20) }]
    set gcd [::danmaku::gcd $a 20]
    set zoom [expr { $a / $gcd }]
    set subsample [expr { 20 / $gcd }]
    return [list $zoom $subsample]
}

proc ::danmaku::image::scale-image {zoom subsample src dest} {
    set temp [image create photo]

    $temp copy $src \
        -zoom $zoom $zoom

    $dest copy $temp \
        -subsample $subsample $subsample

    image delete $temp
}

proc ::danmaku::image::load-scale {zoom subsample file args} {
    set src [image create photo -file $file]
    set dest [image create photo {*}$args]

    scale-image $zoom $subsample $src $dest
    image delete $src

    return $dest
}
